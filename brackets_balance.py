def balance(mystr):
    open_brackets = ['[','{','(']
    closed_brackets = [']','}',')']
    
    mylist = []
    
    for i in mystr:
        
        if i in open_brackets:
            mylist.append(i)
            
        elif i in closed_brackets:
            indx = closed_brackets.index(i)            
            if (len(mylist)>0) and (open_brackets[indx] == mylist[len(mylist)-1]):

                mylist.pop()
            else:
                return False
            
    if len(mylist) == 0:
        return True
    
    else:
        return False
        
    
    
    
    
import unittest

class testing(unittest.TestCase):
    def test(self):
        self.assertTrue(balance("[{([{}])}]"),True)
        self.assertFalse(balance("[{([{)}])}]"),False)

        print('all test cases passed')    
            
            
test = testing()

test.test()

