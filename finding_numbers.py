#finding a number -----(2)


def finding(list1,list2):
    if len(list1) != len(list2)+1:
        return False
    for i in list1:
        if i not in list2:
            
            return i


import unittest

class testing(unittest.TestCase):
    def test(self):
        self.assertEqual(finding([1,2,3,4,5,6,7,8,9,],[4,3,1,6,2,5,7,8]),9)
        self.assertEqual(finding([1,2,3,4,5,6,7,8],[5,2,1,4,6,3,7]),8)
        self.assertEqual(finding([1,2,3,4,5,6],[4,3,1,5,2]),6)

        print('all test cases passed')    
            
            
        

test = testing()
test.test()
