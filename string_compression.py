#string compression -----(4)


def compress(mystr):
    newstr = ''
    num = 1

    newstr +=  mystr[0]

    for i in range(len(mystr)-1):
        if mystr[i] == mystr[i+1]:
            num += 1

        else:
            if num > 1:
                newstr += str(num)
                num = 1

            newstr +=  mystr[i+1]

    if num > 1:
        newstr += str(num)

        return newstr
    
import unittest

class testing(unittest.TestCase):
    def test(self):
        self.assertEqual(compress('aaaassssddddffff'),'a4s4d4f4')
        self.assertEqual(compress('aaaaaeeeerrrrffffdddd'),'a5e4r4f4d4')

        print('all test cases passed')    
            
            
        

test = testing()

test.test()

